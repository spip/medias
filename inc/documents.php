<?php

/**
 * SPIP, Système de publication pour l'internet
 *
 * Copyright © avec tendresse depuis 2001
 * Arnaud Martin, Antoine Pitrou, Philippe Rivière, Emmanuel Saint-James
 *
 * Ce programme est un logiciel libre distribué sous licence GNU/GPL.
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// inclure les fonctions bases du core
include_once _DIR_RESTREINT . 'inc/documents.php';

include_spip('inc/actions'); // *action_auteur et determine_upload

// Constante indiquant le charset probable des documents non utf-8 joints

if (!defined('CHARSET_JOINT')) {
	define('CHARSET_JOINT', 'iso-8859-1');
}

/**
 * Donne le chemin du fichier relatif à `_DIR_IMG`
 * pour stockage 'tel quel' dans la base de données
 *
 * @uses _DIR_IMG
 */
function set_spip_doc(?string $fichier): string {
	if ($fichier && str_starts_with($fichier, (string) _DIR_IMG)) {
		return substr($fichier, strlen((string) _DIR_IMG));
	}
	// ex: fichier distant
	return $fichier ?? '';

}

/**
 * Donne le chemin complet du fichier
 *
 * @uses _DIR_IMG
 *
 * @return bool|string
 */
function get_spip_doc(?string $fichier) {
	$fichier_demande = $fichier;

	if ($fichier === null) {
		return false;
	}

	// fichier distant
	if (tester_url_absolue($fichier)) {
		return $fichier;
	}

	// gestion d'erreurs, fichier=''
	if (!strlen($fichier)) {
		return false;
	}

	if (!str_starts_with($fichier, (string) _DIR_IMG)) {
		$fichier = _DIR_IMG . $fichier;
	}

	$fichier = pipeline('get_spip_doc', [
		'args' => [
			'fichier' => $fichier_demande,
		],
		'data' => $fichier,
	]);

	// fichier normal
	return $fichier;
}

/**
 * Copier un document `$source`
 * dans un dossier `IMG/$ext/$orig.$ext` ou `IMG/$subdir/$orig.$ext` si `$subdir` est fourni
 * en numérotant éventuellement si un fichier de même nom existe déjà
 *
 * @param string $ext
 * @param string $orig
 * @param string $source
 * @param string $subdir
 * @return bool|mixed|string
 */
function copier_document($ext, $orig, $source, $subdir = null) {

	$orig = preg_replace(',\.\.+,', '.', $orig); // pas de .. dans le nom du doc
	$dir = creer_repertoire_documents($subdir ?: $ext);

	$dest = preg_replace('/<[^>]*>/', '', basename($orig));
	$dest = preg_replace('/\.([^.]+)$/', '', $dest);
	$dest = translitteration($dest);
	$dest = preg_replace('/[^.=\w-]+/', '_', (string) $dest);

	// ne pas accepter de noms de la forme -r90.jpg qui sont reserves
	// pour les images transformees par rotation (action/documenter)
	$dest = preg_replace(',-r(90|180|270)$,', '', $dest);

	while (preg_match(',\.(\w+)$,', $dest, $m)) {
		if (
			!function_exists('verifier_upload_autorise')
			|| !($r = verifier_upload_autorise($dest))
			|| !empty($r['autozip'])
		) {
			$dest = substr($dest, 0, -strlen($m[0])) . '_' . $m[1];
			break;
		}
		$dest = substr($dest, 0, -strlen($m[0]));
		$ext = $m[1] . '.' . $ext;

	}

	// Si le document "source" est deja au bon endroit, ne rien faire
	if ($source == ($dir . $dest . '.' . $ext)) {
		return $source;
	}

	// sinon tourner jusqu'a trouver un numero correct
	$n = 0;
	while (@file_exists($newFile = $dir . $dest . ($n++ ? ('-' . $n) : '') . '.' . $ext)) {

	}

	return deplacer_fichier_upload($source, $newFile);
}

/**
 * Efface le répertoire de manière récursive !
 *
 * @param string $nom
 */
function effacer_repertoire_temporaire($nom) {
	if ($d = opendir($nom)) {
		while (($f = readdir($d)) !== false) {
			if (is_file("$nom/$f")) {
				spip_unlink("$nom/$f");
			} else {
				if ($f != '.' && $f != '..' && is_dir("$nom/$f")) {
					effacer_repertoire_temporaire("$nom/$f");
				}
			}
		}
	}
	closedir($d);
	@rmdir($nom);
}

// Filtre pour #FICHIER permettant d'incruster le contenu d'un document
// Si 2e arg fourni, conversion dans le charset du site si possible

function contenu_document($arg, $charset = '') {
	include_spip('inc/distant');
	if (is_numeric($arg)) {
		$r = sql_fetsel('fichier,distant', 'spip_documents', 'id_document=' . intval($arg));
		if (!$r) {
			return '';
		}
		$f = $r['fichier'];
		$f = ($r['distant'] == 'oui') ? _DIR_RACINE . copie_locale($f) : get_spip_doc($f);
	} else {
		if (!@file_exists($f = $arg)) {
			if (!$f = copie_locale($f)) {
				return '';
			}
			$f = _DIR_RACINE . $f;
		}
	}

	$r = spip_file_get_contents($f);

	if ($charset) {
		include_spip('inc/charsets');
		if ($charset !== 'auto') {
			$r = importer_charset($r, $charset);
		} elseif ($GLOBALS['meta']['charset'] == 'utf-8' && !is_utf8($r)) {
			$r = importer_charset($r, CHARSET_JOINT);
		}
	}

	return $r;
}

function generer_url_document_dist($id_document, $args = '', $ancre = '') {

	include_spip('inc/autoriser');
	// si on a pas le droit de voir le document, meme via le htaccess
	if (!autoriser('voir', 'document', $id_document, null, ['htaccess' => true])) {
		return '';
	}

	$r = sql_fetsel('fichier,distant', 'spip_documents', 'id_document=' . intval($id_document));

	if (!$r) {
		return '';
	}

	$f = $r['fichier'];

	if ($r['distant'] == 'oui') {
		return $f;
	}

	// Si droit de voir tous les docs, sans htaccess, pas seulement celui-ci
	// il est inutilement couteux de rajouter une protection
	if (autoriser('voir', 'document')) {
		return get_spip_doc($f);
	}

	include_spip('inc/securiser_action');

	// cette action doit etre publique !
	return generer_url_action(
		'acceder_document',
		$args . ($args ? '&' : '')
			. 'arg=' . $id_document
			. ($ancre ? "&ancre=$ancre" : '')
			. '&cle=' . calculer_cle_action($id_document . ',' . $f)
			. '&file=' . rawurlencode((string) $f),
		true,
		true
	);
}

//
// Affiche le document avec sa vignette par defaut
//
// Attention : en mode 'doc', si c'est un fichier graphique on prefere
// afficher une vue reduite, quand c'est possible (presque toujours, donc)
// En mode 'image', l'image conserve sa taille
//
// A noter : dans le portfolio prive on pousse le vice jusqu'a reduire la taille
// de la vignette -> c'est a ca que sert la variable $portfolio
function vignette_automatique($img, $doc, $lien, $x = 0, $y = 0, $align = '', $class = null, $connect = null) {
	include_spip('inc/distant');
	include_spip('inc/texte');
	include_spip('inc/filtres_images_mini');
	if ($class === null) {
		$class = 'spip_logo spip_logos';
	}
	$e = $doc['extension'] ?? null;
	if (!$img) {
		$img = image_du_document($doc, $connect);
	}
	if (!$img) {
		$f = charger_fonction('vignette', 'inc');
		$img = $f($e, $doc['media'] ?? '');
		$class .= ' spip_document_icone';
	}
	$size = @spip_getimagesize($img);
	$img = "<img src='$img' " . ($size[3] ?? '') . ' />';

	// on appelle image_reduire independamment de la presence ou non
	// des librairies graphiques
	// la fonction sait se debrouiller et faire de son mieux dans tous les cas
	if ($x || $y) {
		$img = image_reduire($img, $x, $y);
	}
	$img = inserer_attribut($img, 'alt', '');
	if ($align) {
		$class .= " spip_logo_$align";
	}
	$img = inserer_attribut($img, 'class', trim((string) $class));

	if (!$lien) {
		return $img;
	}

	$titre = supprimer_tags(typo($doc['titre']));
	$titre = ' - ' . taille_en_octets($doc['taille'])
		. ($titre ? " - $titre" : '');

	$type = sql_fetsel('titre, mime_type', 'spip_types_documents', 'extension = ' . sql_quote($e));

	$mime = $type['mime_type'];
	$titre = attribut_html(couper($type['titre'] . $titre, 80));

	return "<a href='$lien' type='$mime' title='$titre'>$img</a>";
}

/**
 * Trouve une image caractéristique d'un document.
 *
 * Si celui-ci est une image et que les outils graphiques sont dispos,
 * retourner le document (en exploitant sa copie locale s'il est distant).
 *
 * Si on a un connecteur externe, on utilise l’URL externe.
 *
 * Autrement retourner la vignette fournie par SPIP pour ce type MIME
 *
 * @param array $document
 * @param null|string $connect
 * @return string Chemin de l’image
 */
function image_du_document($document, $connect = null) {
	if (
		($e = $document['extension'] ?? null)
		&& in_array($e, formats_image_acceptables())
		&& (!test_espace_prive() || $GLOBALS['meta']['creer_preview'] == 'oui')
		&& $document['fichier']
	) {
		include_spip('public/quete_document');
		if ($document['distant'] == 'oui') {
			$image = _DIR_RACINE . copie_locale($document['fichier']);
		} elseif ($image = document_spip_externe($document['fichier'], $connect)) {
			return $image;
		} else {
			$image = get_spip_doc($document['fichier']);
		}
		if (@file_exists($image)) {
			return $image;
		}
	}

	return '';
}

/**
 * Affiche le code d'un raccourcis de document, tel que <doc123|left>
 *
 * Affiche un code de raccourcis de document, et l'insère
 * dans le textarea principal de l'objet (champ 'texte') sur un double-clic
 *
 * @param string $doc
 *    Type de raccourcis : doc,img,emb...
 * @param int $id
 *    Identifiant du document
 * @param string $align
 *    Alignement du document : left,center,right
 * @param bool $short
 *    Réduire le texte affiché à la valeur de 'align'
 *
 * @return string
 *    Texte du raccourcis
 */
function affiche_raccourci_doc($doc, $id, $align = '', $short = false) {

	$pipe = '';
	if ($align) {
		$pipe = "|$align";
	}

	$model = "&lt;$doc$id$pipe&gt;";
	$text = $model;
	if ($short) {
		$text = $align ?: $model;
	}

	$classes = 'btn btn_link btn_mini';
	$classes = " class=\"$classes\"";

	$styles = '';
	if ($align && !$short) {
		// a priori ne sert plus de toutes façons…
		$styles = 'text-align: ' . ($align ?: 'center') . ';';
		$styles = " style=\"$styles\"";
	}

	$js = "barre_inserer('$model'); return false;";
	$js = " onmousedown=\"$js\"";

	$title = attribut_html(_T('medias:inserer_raccourci'));
	$title = " title=\"$title\"";

	return "\n<button{$classes}{$styles}{$js}{$title}>$text</button>\n";
}
