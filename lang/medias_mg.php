<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/medias?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// L
	'label_activer_document_objets' => 'Activer le téléversement pour les contenus :',
	'label_alt' => 'Alternative textuelle (alt)',
	'label_apercu' => 'Aperçu',
	'label_caracteristiques' => 'Caractéristiques',
	'label_credits' => 'Crédits',
	'label_fichier' => 'Fichier',
	'label_fichier_vignette' => 'Vignette',
	'label_paginer_par' => 'Paginer par :',
	'label_parents' => 'Ce document est lié à',
	'label_refdoc_joindre' => 'Document numéro',
	'label_titre_mimetype' => 'Type de fichier',
	'lien_tout_desordonner' => 'Réinitialiser l’ordre',
	'lien_tout_desordonner_verif' => 'L’ordre définit pour cette liste de documents sera réinitialisé. Êtes-vous sûr ?',
	'lien_tout_enlever' => 'Tout détacher',
	'lien_tout_enlever_verif' => 'Voulez-vous détacher tous les documents ?',
	'logo' => 'Logo',

	// M
	'media_audio' => 'Bandes-son',
	'media_file' => 'Autres',
	'media_image' => 'Images',
	'media_video' => 'Séquences',
	'miniature_automatique_active' => 'SPIP génèrera automatiquement une miniature de l’image',

	// N
	'nb_documents_attache_succes' => '@nb@ documents ont bien été ajoutés',
	'nb_documents_installe_succes' => '@nb@ fichiers chargés avec succès',

	// O
	'objet_document' => 'Document',
	'objet_documents' => 'Documents',
	'ordonner_ce_document' => 'Ordonner ce document',

	// P
	'par_date' => 'Date',
	'par_hauteur' => 'Hauteur',
	'par_id' => 'ID',
	'par_largeur' => 'Largeur',
	'par_taille' => 'Poids',
	'par_titre' => 'Titre',

	// T
	'texte_documents_joints' => 'Vous pouvez activer l’interface d’ajout de documents (fichiers bureautiques, images, multimédia, etc.) aux articles, rubriques et autres. Ces fichiers peuvent ensuite être référencés dans le texte, ou affichés séparément.',
	'texte_documents_joints_2' => 'Ce réglage n’empêche pas le téléversement d’images dans les articles ni l’insertion de document directement dans les textes des contenus.',
	'titre_documents_joints' => 'Documents joints',
	'titre_page_documents_edit' => 'Modifier le document : @titre@',
	'tous_les_medias' => 'Tous les médias',
	'tout_dossier_upload' => 'Tout le dossier @upload@',
	'tout_voir' => 'Tout voir',

	// U
	'un_audio' => '1 bande-son',
	'un_document' => '1 document',
	'un_file' => '1 document',
	'un_image' => '1 image',
	'un_video' => '1 séquence',
	'une_utilisation' => '1 utilisation',
	'upload_fichier_zip' => 'Fichier ZIP',
	'upload_fichier_zip_texte' => 'Le fichier que vous proposez d’installer est un fichier Zip.',
	'upload_fichier_zip_texte2' => 'Ce fichier peut être :',
	'upload_info_mode_document' => 'Déposer dans le portfolio',
	'upload_info_mode_image' => 'Retirer du portfolio',
	'upload_limit' => 'Ce fichier est trop gros pour le serveur ; la taille maximum autorisée en <i>upload</i> est de @max@.',
	'upload_zip_conserver' => 'Conserver l’archive après extraction',
	'upload_zip_decompacter' => 'décompressé et chaque élément qu’il contient installé sur le site. Les fichiers qui seront alors installés sur le site sont :',
	'upload_zip_mode_document' => 'Déposer toutes les images dans le portfolio',
	'upload_zip_telquel' => 'installé tel quel, en tant qu’archive compressée Zip ;',
	'upload_zip_titrer' => 'Titrer selon le nom des fichiers',

	// V
	'verifier_documents_brises' => 'Vérifier les fichiers manquants',
	'verifier_documents_inutilises' => 'Vérifier les liens des documents',
	'vignette_supprimee' => 'La vignette a été supprimée',
];
